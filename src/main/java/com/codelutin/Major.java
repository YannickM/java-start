package com.codelutin;

public interface Major {

    boolean isMajor();
}
