package com.codelutin;

public interface Musician {

    void playMusic();

    void recordAlbum();

}
