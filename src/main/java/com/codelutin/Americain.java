package com.codelutin;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Americain extends Person implements Major {

    public Americain(String firstname, String name, int age) {
        super(firstname, name);
        this.age = age;
    }

    @Override
    public boolean isMajor() {
        return age >= 21;
    }
}
