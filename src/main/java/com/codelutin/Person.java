package com.codelutin;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Person implements Comparable<Person> {

    protected String prenom;
    protected String nom;
    protected int age;

    public Person(String firstname, String name) {
        this.prenom = firstname;
        this.nom = name;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nouveauNom) {
        this.nom = nouveauNom;
    }

    public void sayHello() {
        System.out.println("Bonjour !");
    }

    public void presentation() {
        System.out.println("Je m'appelle " + this.prenom + " " + this.nom);
    }

    @Override
    public int compareTo(Person p) {
        return this.prenom.compareTo(p.getPrenom());
    }
}
