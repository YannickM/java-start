package com.codelutin;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Guitarist extends Person implements Musician {

    public Guitarist(String firstname, String name) {
        super(firstname, name);
    }

    @Override
    public void sayHello() {
        System.out.println("Yeaaaaaaaaaaaaaaaaaahhhh");
    }

    @Override
    public void playMusic() {
        System.out.println("play guitar");
    }

    @Override
    public void recordAlbum() {

    }
}
