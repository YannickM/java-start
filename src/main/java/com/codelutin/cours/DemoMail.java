package com.codelutin.cours;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class DemoMail {

    public static void main(String[] args) {

        Email courriel = new Email("Cours Java", "blablablabla", "IDP Java 20", "Yannick"); // coquille vide
        courriel.afficher();

        // Remplir les données de l'email
//        email.setTitre("Cours Java");
//        email.setExpediteur("martel yannick");
        courriel.setDestinataire("IDP Java 2020");
//        email.attribuerContenu("Blablablabla");

        // Manipule l'instance
        courriel.send();
        courriel.afficher();
    }

}
