package com.codelutin.cours;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Revue implements Comparable<Revue> {

    protected String title;
    protected int number;

    public Revue(String title, int number) {
        this.title = title;
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public int getNumber() {
        return number;
    }

    public String toString() {
        return title + " " + number;
    }

    @Override
    public int compareTo(Revue other) {
        return title.compareTo( other.getTitle());
    }
}
