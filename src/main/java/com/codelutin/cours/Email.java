package com.codelutin.cours;

import java.time.LocalDate;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Email {

    private String titre;
    private String contenu;
    private String destinataire;
    private String expediteur;
    private LocalDate sendDate;

    public Email() {
        this.titre = "(empty)";
        this.destinataire = "N/A";
        this.expediteur = "N/A";
        this.contenu = "";
        this.sendDate = LocalDate.now();
    }

    public Email(String titre, String contenu, String destinataire, String expediteur) {
        this.titre = titre;
        this.contenu = contenu;
        this.destinataire = destinataire;
        this.expediteur = expediteur;
        this.sendDate = LocalDate.now();
    }

    public void setTitre(String titre) {
        if (titre == null || titre.isEmpty() ) {
            throw new RuntimeException("vous avez fourni un titre vide !");
        }
        this.titre = titre;
    }

    public void attribuerContenu(String contenu) {
        this.contenu = contenu;
    }

    public void setDestinataire(String destinataire) {
        this.destinataire = destinataire;
    }

    public void setExpediteur(String expediteur) {
        this.expediteur = expediteur;
    }

    public void afficher() {
        System.out.println("From : " + expediteur);
        System.out.println("To : " + destinataire);
        System.out.println("Subject : " + titre);
        System.out.println("Content : " + contenu);
        System.out.println("Date : " + sendDate);
    }

    public void send() {
        // Se connecter au réseau
        // Envoyer l'email
    }
}
