package com.codelutin.cours;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeSet;

/**
 * Hello world!
 */
public class Demo {

    public static void main(String[] args) {

        TreeSet<Revue> revues = new TreeSet<>();
        // les éléments seront triés sur l'ordre naturel
        // basé sur le surcharge de l'interface Comparable

        revues.add(new Revue("Geo", 1));
        revues.add(new Revue("CQ", 2));
        revues.add(new Revue("France Football", 3));
        revues.add(new Revue("Linux Pratique", 4));
        revues.add(new Revue("Science et vie", 1));
        revues.add(new Revue("Science et vie", 2));
        revues.add(new Revue("Science et vie", 3));

        for (Revue revue : revues) {
            System.out.println(revue);
        }

        System.out.println("Number comparaison");
        NumberComparator numberComparator = new NumberComparator();
        revues = new TreeSet<>(numberComparator);

        revues.add(new Revue("Linux Pratique", 4));
        revues.add(new Revue("Geo", 1));
        revues.add(new Revue("France Football", 3));
        revues.add(new Revue("CQ", 2));
        revues.add(new Revue("Science et vie", 1));
        revues.add(new Revue("Science et vie", 2));
        revues.add(new Revue("Science et vie", 3));

        for (Revue revue : revues) {
            System.out.println(revue);
        }

        System.out.println("====================");
        System.out.println("====================");
        System.out.println("====================");
        System.out.println("====================");
        List<Revue> listRevues = new ArrayList<>();

        listRevues.add(new Revue("Science et vie", 1));
        listRevues.add(new Revue("Science et vie", 2));
        listRevues.add(new Revue("Science et vie", 3));
        listRevues.add(new Revue("Linux Pratique", 4));
        listRevues.add(new Revue("Geo", 1));
        listRevues.add(new Revue("France Football", 3));
        listRevues.add(new Revue("CQ", 2));

        Collections.sort(listRevues, numberComparator);

        for (Revue listRevue : listRevues) {
            System.out.println(listRevue);
        }
    }
}
