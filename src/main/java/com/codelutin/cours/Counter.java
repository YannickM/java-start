package com.codelutin.cours;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Counter {

    private static int nbInstances;

    public Counter() {
        this.nbInstances ++;
    }

    public static int howManyInstances() {
        return nbInstances;
    }

    public int getNbInstances() {
        return nbInstances;
    }

    public void finalize() {
        this.nbInstances --;
    }
}
