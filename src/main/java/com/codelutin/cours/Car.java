package com.codelutin.cours;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Car {

    private String color;
    private String brand;
    public int power;

    public Car() {

    }

    public Car(String color, String brand, int power) {
        this.color = color;
        this.brand = brand;
        this.power = power;
    }

    public void modifer(String newColor, String newBrand, int newPower) { // modifier(String, String, int)
        this.color = newColor;
        this.brand = newBrand;
        this.power = newPower;
    }

    public void modifer(String newBrand, int newPower) { // modifier(String, String, int)

        this.brand = newBrand;
        this.power = newPower;
        this.color = "royal blue";
    }

    public void setColor(String newColor) { // modifier(String)
        this.color = newColor;
    }

    public void setPower(int power) {
        if (power < 1) {
            throw new RuntimeException("Mini puissance = 2");
        }
        this.power = power;
    }

    public void modifer(Car car) {
        this.color = car.color;
        this.brand = car.brand;
        this.power = car.power;
    }

    public String getColor() {
        return color;
    }


}
