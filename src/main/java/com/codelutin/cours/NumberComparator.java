package com.codelutin.cours;

import java.util.Comparator;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class NumberComparator implements Comparator<Revue> {

    @Override
    public int compare(Revue o1, Revue o2) {
        return Integer.compare(o1.getNumber(), o2.getNumber());
    }

}
