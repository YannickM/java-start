package com.codelutin;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Example {

    public static void main(String[] args) {

        Person yannick = new Person("Yannick", "Martel");

        Person laetitia = new Person("Laetitia", "R.");

        List<Person> persons = new ArrayList<>();
        persons.add(yannick);
        persons.add(laetitia);

        Guitarist guitarist = new Guitarist("Jimmy", "Hendrix");

        persons.add(guitarist);

        System.out.println("Liste de mes personnes");
        for (Person person : persons) {
            person.sayHello();
            person.presentation();
        }

        PersonNameComparator personNameComparator = new PersonNameComparator();

        Collections.sort(persons, personNameComparator);
        Collections.sort(persons, Comparator.comparing(Person::getNom));

        System.out.println("Liste de mes personnes triée par nom");
        for (Person person : persons) {
            person.presentation();
        }

        Collections.sort(persons);

        System.out.println("Liste de mes personnes triée par prénom");
        for (Person person : persons) {
            person.presentation();
        }

    }

}
