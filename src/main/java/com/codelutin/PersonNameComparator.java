package com.codelutin;

import java.util.Comparator;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class PersonNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person p1, Person p2) {

        //    p1  =  p2
        //        0         égalité : curseur sur le =
        //    -1  .   .     curseur sur p1 : p1 est avant p2
        //           1      curseur sur p2 : p2 est avant p1

        String p1Nom = p1.getNom();
        String p2Nom = p2.getNom();

        int res = p1Nom.compareTo(p2Nom);

        return res;
    }

}
