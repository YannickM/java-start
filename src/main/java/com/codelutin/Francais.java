package com.codelutin;

/**
 * @author ymartel (martel@codelutin.com)
 */
public class Francais extends Person implements Major {

    public Francais(String firstname, String name, int age) {
        super(firstname, name);
        this.age = age;
    }

    @Override
    public boolean isMajor() {
        return age >= 18;
    }
}
